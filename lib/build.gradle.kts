plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version "1.6.21"
    id("com.android.library")
    `maven-publish`
}

group = "me.aguragorn.pixelfed4kmpp"
version = "1.0.0-SNAPSHOT"

kotlin {
    android()
    jvm("desktop") {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }

    sourceSets {
        val ktorVersion = "2.0.0"
        val mockkVersion = "1.12.3"

        getByName("commonMain") {
            dependencies {
                implementation("io.ktor:ktor-client-auth:$ktorVersion")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                implementation("io.ktor:ktor-client-logging:$ktorVersion")
                api("io.ktor:ktor-client-okhttp:$ktorVersion")
                implementation("io.ktor:ktor-client-resources:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")

                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
            }
        }
        getByName("commonTest") {
            dependencies {
                implementation(kotlin("test"))
                implementation("io.ktor:ktor-client-mock:$ktorVersion")
                implementation("io.mockk:mockk-common:$mockkVersion")
            }
        }

        getByName("androidMain")
        getByName("androidTest") {
            dependencies {
                implementation("io.mockk:mockk:$mockkVersion")
            }
        }

        getByName("desktopMain")
        getByName("desktopTest") {
            dependencies {
                implementation("io.mockk:mockk:$mockkVersion")
            }
        }
    }
}

android {
    compileSdkVersion(31)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(31)
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

//repositories {
//    maven {
//        url = uri("https://gitlab.example.com/api/v4/projects/35740806/packages/maven")
//        credentials(HttpHeaderCredentials::class) {
//            name = "Job-Token"
//            value = System.getenv("CI_JOB_TOKEN")
//        }
//        authentication {
//            create<HttpHeaderAuthentication>("header")
//        }
//    }
//}