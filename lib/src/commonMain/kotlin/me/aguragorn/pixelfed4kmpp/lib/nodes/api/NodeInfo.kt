package me.aguragorn.pixelfed4kmpp.lib.nodes.api

import io.ktor.client.call.*
import io.ktor.client.request.*
import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.nodes.model.NodeInfo
import me.aguragorn.pixelfed4kmpp.lib.utils.resolveOn

suspend fun PixelfedPublicClient.getNodeInfo(
    schemaUrl: String
): NodeInfo {
    val urlString = schemaUrl.resolveOn(instanceName)
    return client.get(urlString = urlString).body()
}
