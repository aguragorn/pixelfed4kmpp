package me.aguragorn.pixelfed4kmpp.lib.utils

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.resources.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import me.aguragorn.pixelfed4kmpp.lib.authentication.api.refreshToken
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Secrets
import me.aguragorn.pixelfed4kmpp.lib.authentication.store.SecretsStore

internal interface HttpClientFactory {
    fun newInstance(): HttpClient
}

internal class PublicHttpClientFactory(
    private val instanceName: String,
    private val extension: OkHttpConfig.() -> Unit = {},
) : HttpClientFactory {
    override fun newInstance(): HttpClient = HttpClient(OkHttp) {
        defaults()
        defaultRequest {
            host = instanceName
            url { protocol = URLProtocol.HTTPS }
            contentType(ContentType.Application.Json)
        }
        engine { extension() }
    }
}

internal class SecureHttpClientFactory(
    private val instanceName: String,
    private val secretsStore: SecretsStore,
    private val extension: OkHttpConfig.() -> Unit = {},
) : HttpClientFactory {
    override fun newInstance(): HttpClient = HttpClient(OkHttp) {
        defaults()
        install(Auth) {
            bearer {
                loadTokens {
                    val secrets = secretsStore.get()
                    BearerTokens(
                        accessToken = secrets.lastToken.access_token.orEmpty(),
                        refreshToken = secrets.lastToken.refresh_token.orEmpty()
                    )
                }
                refreshTokens {
                    val secrets = secretsStore.get()
                    val token = client.refreshToken(
                        clientId = secrets.clientId,
                        clientSecret = secrets.clientSecret,
                        refreshToken = secrets.lastToken.refresh_token
                    )
                    secretsStore.save(
                        Secrets(
                            clientId = secrets.clientId,
                            clientSecret = secrets.clientSecret,
                            lastToken = token
                        )
                    )
                    BearerTokens(
                        accessToken = token.access_token.orEmpty(),
                        refreshToken = token.refresh_token.orEmpty()
                    )
                }
            }
        }
        defaultRequest {
            host = instanceName
            url { protocol = URLProtocol.HTTPS }
            contentType(ContentType.Application.Json)
        }
        engine { extension() }
    }
}

internal fun HttpClientConfig<*>.defaults() {
    install(Resources)
    install(ContentNegotiation) {
        json(Json {
            ignoreUnknownKeys = true
            isLenient = true
        })
    }
    expectSuccess = true
}