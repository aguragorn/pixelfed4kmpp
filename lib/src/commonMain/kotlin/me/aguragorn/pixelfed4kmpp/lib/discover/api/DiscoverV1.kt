package me.aguragorn.pixelfed4kmpp.lib.discover.api

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/api/v1/discover")
class DiscoverV1 {

    @Serializable
    @Resource("posts")
    class Posts(val parent: DiscoverV1)
}