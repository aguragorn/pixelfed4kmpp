package me.aguragorn.pixelfed4kmpp.lib

import io.ktor.client.engine.okhttp.*
import me.aguragorn.pixelfed4kmpp.lib.utils.HttpClientFactory
import me.aguragorn.pixelfed4kmpp.lib.utils.PublicHttpClientFactory

/**
 * Contains API for calling public endpoints.
 */
class PixelfedPublicClient internal constructor(
    /**
     * ie. pixelfed.social
     */
    val instanceName: String,
    httpClientFactory: HttpClientFactory,
) : PixelfedClient {
    constructor(
        instanceName: String,
        extension: OkHttpConfig.() -> Unit = {},
    ) : this(
        instanceName = instanceName,
        httpClientFactory = PublicHttpClientFactory(instanceName = instanceName, extension = extension),
    )

    override val client = httpClientFactory.newInstance()
}
