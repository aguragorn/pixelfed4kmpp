package me.aguragorn.pixelfed4kmpp.lib.application.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedClient
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.application.model.Application
import me.aguragorn.pixelfed4kmpp.lib.application.model.RegisterAppParams

@Serializable
@Resource("/api/v1/apps")
internal class ApplicationsV1 {

    /**
     * Must be called from a [PixelfedSecureClient]
     */
    @Serializable
    @Resource("verify_credentials")
    internal class VerifyCredentials(val parent: ApplicationsV1 = ApplicationsV1())
}

const val default_scopes: String = "read"

/**
 * Register an application, in order to be able to generate access
 * tokens later.
 */
suspend fun PixelfedClient.registerApp(
    params: RegisterAppParams
): Application {
    return client.post(ApplicationsV1()) {
        setBody(params)
    }.body()
}

/**
 * Verify that our obtained credentials are working
 */
suspend fun PixelfedSecureClient.verifyAppCredentials(): Application {
    return client.post(ApplicationsV1.VerifyCredentials()).body()
}