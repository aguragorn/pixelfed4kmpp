package me.aguragorn.pixelfed4kmpp.lib.nodes.model

import kotlinx.serialization.Serializable

@Serializable
data class NodeInfoJRD(
    val links: List<Link>
) {
    @Serializable
    data class Link(
        val rel: String?,
        val href: String?
    )

}