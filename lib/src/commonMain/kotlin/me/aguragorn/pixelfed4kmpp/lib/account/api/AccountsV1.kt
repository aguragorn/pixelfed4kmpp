package me.aguragorn.pixelfed4kmpp.lib.account.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedClient
import me.aguragorn.pixelfed4kmpp.lib.account.model.Account
import me.aguragorn.pixelfed4kmpp.lib.account.model.Relationship
import me.aguragorn.pixelfed4kmpp.lib.content.model.Status

@Serializable
@Resource("/api/v1/accounts")
internal class AccountsV1 {

    @Serializable
    @Resource("verify_credentials")
    class VerifyCredentials(val parent: AccountsV1 = AccountsV1())

    @Serializable
    @Resource("relationships")
    class Relationships(
        val parent: AccountsV1 = AccountsV1(),
        val id: List<String>
    )

    @Serializable
    @Resource("search")
    class Search(val parent: AccountsV1 = AccountsV1())

    @Serializable
    @Resource("{id}")
    class Id(
        val parent: AccountsV1 = AccountsV1(),
        val id: String
    ) {

        @Serializable
        @Resource("statuses")
        class Statuses(
            val parent: Id,
            val min_id: String? = null,
            val max_id: String? = null,
            val limit: Int? = null,
        )

        @Serializable
        @Resource("followers")
        class Followers(
            val parent: Id,
            val max_id: String? = null,
            val since_id: String? = null,
            val limit: Int? = null,
            val page: String? = null
        )

        @Serializable
        @Resource("following")
        class Following(
            val parent: Id,
            val max_id: String? = null,
            val since_id: String? = null,
            val limit: Int? = 40,
            val page: String? = null
        )

        @Serializable
        @Resource("follow")
        class Follow(val parent: Id)

        @Serializable
        @Resource("unfollow")
        class Unfollow(val parent: Id)

        @Serializable
        @Resource("block")
        class Block(val parent: Id)

        @Serializable
        @Resource("unblock")
        class Unblock(val parent: Id)

        @Serializable
        @Resource("mute")
        class Mute(val parent: Id)

        @Serializable
        @Resource("unmute")
        class Unmute(val parent: Id)

        @Serializable
        @Resource("pin")
        class Pin(val parent: Id)

        @Serializable
        @Resource("unpin")
        class Unpin(val parent: Id)

        @Serializable
        @Resource("featured_tags")
        class FeaturedTags(val parent: Id)

        @Serializable
        @Resource("lists")
        class Lists(val parent: Id)

        @Serializable
        @Resource("identity_proofs")
        class IdentityProofs(val parent: Id)

        @Serializable
        @Resource("note")
        class Note(val parent: Id)
    }
}

// TODO: Register account
// TODO: Update credentials

suspend fun PixelfedClient.verifyCredentials(): Account {
    return client.get(AccountsV1.VerifyCredentials()).body()
}

/**
 * View information about a profile.
 *
 * @param id The id of the account in the database
 */
suspend fun PixelfedClient.getAccount(
    id: String
): Account {
    return client.get(
        AccountsV1.Id(id = id)
    ).body()
}

suspend fun PixelfedClient.getRelationships(
    accountIds: List<String>
): List<Relationship> {
    return client.get(
        AccountsV1.Relationships(id = accountIds)
    ).body()
}

/**
 * Statuses posted to the given account.
 *
 * @param id The id of the account in the database
 */
suspend fun PixelfedClient.getStatuses(
    id: String,
    minId: String? = null,
    maxId: String? = null,
    limit: Int? = null,
): List<Status> {
    return client.get(
        AccountsV1.Id.Statuses(
            AccountsV1.Id(id = id),
            min_id = minId,
            max_id = maxId,
            limit = limit
        )
    ).body()
}

/**
 * Accounts which follow the given account, if network is not hidden by the account
 * owner.
 *
 * @param id The id of the account in the database.
 * @param maxId Used for pagination.
 * @param sinceId Used for pagination.
 * @param limit Maximum number of results to return. Defaults to 40.
 */
suspend fun PixelfedClient.getFollowers(
    id: String,
    maxId: String? = null,
    sinceId: String? = null,
    limit: Int? = null,
    page: String? = null
): List<Account> {
    return client.get(
        AccountsV1.Id.Followers(
            AccountsV1.Id(id = id),
            max_id = maxId,
            since_id = sinceId,
            limit = limit,
            page = page
        )
    ).body()
}

/**
 * Accounts which the given account is following, if network is not hidden
 * by the account owner.
 *
 * @param Id The id of the account in the database
 * @param maxId Used for pagination.
 * @param sinceId Used for pagination.
 * @param limit Maximum number of results to return. Defaults to 40.
 */
suspend fun PixelfedClient.getFollowing(
    accountId: String,
    maxId: String? = null,
    sinceId: String? = null,
    limit: Int? = 40,
    page: String? = null
): List<Account> {
    return client.get(
        AccountsV1.Id.Following(
            AccountsV1.Id(id = accountId),
            max_id = maxId,
            since_id = sinceId,
            limit = limit,
            page = page
        )
    ).body()
}

suspend fun PixelfedClient.follow(
    accountId: String,
    reblogs: Boolean = true
): Relationship {
    return client.post(
        AccountsV1.Id.Follow(AccountsV1.Id(id = accountId)),
    ) {
        setBody(
            FormDataContent(
                Parameters.build {
                    append("reblogs", "$reblogs")
                }
            )
        )
    }.body()
}

suspend fun PixelfedClient.unfollow(
    accountId: String
): Relationship {
    return client.post(
        AccountsV1.Id.Unfollow(AccountsV1.Id(id = accountId))
    ).body()
}