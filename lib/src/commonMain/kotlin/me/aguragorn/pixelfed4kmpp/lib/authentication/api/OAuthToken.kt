package me.aguragorn.pixelfed4kmpp.lib.authentication.api

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.application.api.default_scopes
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.AuthenticateAccountParams
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.AuthenticateAppParams
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Token
import java.net.URI

@Serializable
@Resource("/oauth/token")
internal class OAuthToken

/**
 * Used to obtain an application access token that will authenticate our requests.
 * The token will be stored in the [PixelfedSecureClient.secretsStore].
 */
suspend fun PixelfedPublicClient.authenticateApp(
    params: AuthenticateAppParams
): Token {
    return client.post(OAuthToken()) {
        setBody(params)
    }.body()
}

/**
 * Used to obtain a user access token that will authenticate our requests.
 * The token will be stored in the [PixelfedSecureClient.secretsStore].
 */
suspend fun PixelfedPublicClient.authenticateAccount(
    params: AuthenticateAccountParams
): Token {
    return client.post(OAuthToken()) {
        setBody(params)
    }.body()
}

/**
 * Returns a [URI] that can be used to authorize an application to
 * access the api of an instance on behalf of the user. This must be
 * opened in a browser.
 */
fun PixelfedPublicClient.getAuthorizationUrl(
    clientId: String,
    redirectUri: String,
    scopes: String = default_scopes
): URI {

    return URLBuilder(host = instanceName).apply {
        path("oauth/authorize")
        parameters["client_id"] = clientId
        parameters["redirect_uri"] = redirectUri
        parameters["scope"] = scopes
        parameters["response_type"] = "code"
    }.build().toURI()
}

fun PixelfedPublicClient.extractAuthorizationCode(
    oauthRedirectUri: String
): String {
    return Url(oauthRedirectUri).parameters["code"].orEmpty()
}

internal suspend fun HttpClient.refreshToken(
    clientId: String,
    clientSecret: String,
    refreshToken: String? = null,
): Token {
    return post(OAuthToken()) {
        setBody(FormDataContent(
            Parameters.build {
                this["client_id"] = clientId
                this["client_secret"] = clientSecret
                this["scope"] = ""
                this["grant_type"] = "refresh_token"
                refreshToken?.let { this["refresh_token"] = it }
            }
        ))
    }.body()
}