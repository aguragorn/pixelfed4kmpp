package me.aguragorn.pixelfed4kmpp.lib

import io.ktor.client.engine.okhttp.*
import me.aguragorn.pixelfed4kmpp.lib.authentication.store.SecretsStore
import me.aguragorn.pixelfed4kmpp.lib.utils.HttpClientFactory
import me.aguragorn.pixelfed4kmpp.lib.utils.SecureHttpClientFactory

/**
 * Contains API for calling endpoints with authentication ie. endpoints for
 * registered applications, endpoints for logged-in account.
 */
class PixelfedSecureClient internal constructor(
    /**
     * ie. pixelfed.social
     */
    val instanceName: String,
    val secretsStore: SecretsStore,
    httpClientFactory: HttpClientFactory,
) : PixelfedClient {
    constructor(
        instanceName: String,
        secretsStore: SecretsStore,
        extension: OkHttpConfig.() -> Unit = {},
    ) : this(
        instanceName = instanceName,
        secretsStore = secretsStore,
        httpClientFactory = SecureHttpClientFactory(
            instanceName = instanceName,
            secretsStore = secretsStore,
            extension = extension,
        )
    )

    override val client = httpClientFactory.newInstance()
}