package me.aguragorn.pixelfed4kmpp.lib.content.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.content.model.Status

@Serializable
@Resource("/api/v1/statuses")
internal class StatusesV1 {

    @Serializable
    @Resource("{id}")
    internal class Id(val parent: StatusesV1, val id: String) {

        @Serializable
        @Resource("favourite")
        internal class Favourite(val parent: Id)

        @Serializable
        @Resource("unfavourite")
        internal class Unfavourite(val parent: Id)

        @Serializable
        @Resource("reblog")
        internal class Reblog(val parent: Id)

        @Serializable
        @Resource("unreblog")
        internal class Unreblog(val parent: Id)

        @Serializable
        @Resource("context")
        internal class Context(val parent: Id)
    }
}

suspend fun PixelfedSecureClient.post(status: Status): Status {
    return client.post(StatusesV1()) {
        setBody(
            FormDataContent(
                Parameters.build {

                }
            )
        )
    }.body()
}