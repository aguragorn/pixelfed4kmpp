package me.aguragorn.pixelfed4kmpp.lib.content.model

import kotlinx.serialization.Serializable

@Serializable
data class History(
    //Required attributes
    val day: String,
    val uses: String,
    val accounts: String
)