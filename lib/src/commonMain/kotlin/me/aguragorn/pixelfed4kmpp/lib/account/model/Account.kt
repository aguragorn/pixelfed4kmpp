package me.aguragorn.pixelfed4kmpp.lib.account.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.content.model.Emoji
import me.aguragorn.pixelfed4kmpp.lib.content.model.Field
import me.aguragorn.pixelfed4kmpp.lib.content.model.Source

@Serializable
data class Account(
    //Base attributes
    /**
     * The account id.
     */
    val id: String,
    val username: String,
    val acct: String,
    /**
     * The location of the user’s profile page.
     */
    val url: String, //HTTPS URL
    //Display attributes
    val display_name: String? = "",
    val note: String? = "", //HTML
    val avatar: String? = "", //URL
    val avatar_static: String? = "", //URL
    val header: String? = "", //URL
    val header_static: String? = "", //URL
    val locked: Boolean? = false,
    val emojis: List<Emoji>? = null,
    val discoverable: Boolean? = true,
    //Statistical attributes
    val created_at: Instant? = null, //ISO 8601 Datetime
    val statuses_count: Int? = 0,
    val followers_count: Int? = 0,
    val following_count: Int? = 0,
    //Optional attributes
    val moved: Account? = null,
    val fields: List<Field>? = emptyList(),
    val bot: Boolean? = false,
    val group: Boolean? = false,
    val source: Source? = null,
)
