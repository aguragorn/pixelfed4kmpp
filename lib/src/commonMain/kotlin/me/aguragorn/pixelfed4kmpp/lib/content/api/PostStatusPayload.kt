package me.aguragorn.pixelfed4kmpp.lib.content.api

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.content.model.Visibility

/**
 * Request body for posting a status.
 */
@Serializable
data class PostStatusPayload(
    /**
     * Text content of the status. If [media_ids] is provided, this becomes optional.
     * Attaching a [poll] is optional while status is provided.
     */
    val status: String? = null,
    /**
     * Array of Attachment ids to be attached as media. If provided, [status] becomes
     * optional, and [poll] cannot be used.
     */
    val media_ids: List<String>? = null,
    /**
     * If provided, [media_ids] cannot be used
     */
    val poll: Poll? = null,
    /**
     * ID of the status being replied to, if status is a reply
     */
    val in_reply_to_id: String? = null,
    /**
     * Mark status and attached media as sensitive?
     */
    val sensitive: Boolean? = null,
    /**
     * Text to be shown as a warning or subject before the actual content. Statuses
     * are generally collapsed behind this field.
     */
    val spoiler_text: String? = null,
    /**
     * Visibility of the posted status.
     */
    val visibility: Visibility? = null,
    /**
     * Datetime at which to schedule a status. Providing this paramter will cause
     * ScheduledStatus to be returned instead of Status. Must be at least 5 minutes
     * in the future.
     */
    val scheduled_at: Instant? = null,
    /**
     * ISO 639 language code for this status.
     */
    val language: String? = null,
) {

    @Serializable
    data class Poll(
        /**
         * Array of possible answers.
         */
        val options: List<String>,
        /**
         * Duration the poll should be open, in seconds.
         */
        val expires_in: Long,
        /**
         * Allow multiple choices?
         */
        val multiple: Boolean,
        /**
         * Hide vote counts until the poll ends?
         */
        val hide_totals: Boolean,
    )
}
