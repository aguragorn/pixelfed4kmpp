package me.aguragorn.pixelfed4kmpp.lib.application.model

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.application.api.default_scopes

/**
 * Contains parameters for sending a request for registering the application to the
 * instance.
 *
 * @param client_name Will be shown on statuses if applicable.
 * @param redirect_uris When generating the app token, we will need to provide a URI
 * that isincluded within this list.
 * @param scopes Allow us to define what permissions we can request later. However,
 * the requested scope later can be a subset of these registered scopes.
 * @param website Will be shown on statuses if applicable.
 */
@Serializable
data class RegisterAppParams(
    val client_name: String,
    val redirect_uris: String,
    @EncodeDefault
    val scopes: String = default_scopes,
    val website: String? = null,
)