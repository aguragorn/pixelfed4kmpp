package me.aguragorn.pixelfed4kmpp.lib.application.model

import kotlinx.serialization.Serializable

@Serializable
data class Instance(
    val description: String?,
    val email: String?,
    val max_toot_chars: String? = DEFAULT_MAX_TOOT_CHARS.toString(),
    val registrations: Boolean?,
    val thumbnail: String?,
    val title: String?,
    val uri: String?,
    val version: String?
) {
    companion object {
        // Default max number of chars for Mastodon: used when there is no other value supplied by
        // either NodeInfo or the instance endpoint
        const val DEFAULT_MAX_TOOT_CHARS = 500

        const val DEFAULT_MAX_PHOTO_SIZE = 8000
        const val DEFAULT_MAX_VIDEO_SIZE = 40000
        const val DEFAULT_ALBUM_LIMIT = 4
        const val DEFAULT_VIDEO_ENABLED = true
    }
}