package me.aguragorn.pixelfed4kmpp.lib.authentication.model

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.Serializable

/**
 * Parameters to authenticate account.
 *
 * @param client_id generated when application was registered.
 * @param client_secret generated when application was registered.
 * @param redirect_uri must be one of the URIs defined when registering the
 * application.
 * @param code The code can only be used once. If you need to obtain a new token,
 * you will need to have the user authorize again by repeating the above Authorize
 * the user step.
 * @param scope We are requesting a grant_type of authorization_code, which still
 * defaults to giving us the read scope. However, while authorizing our user, we
 * requested a certain scope – pass the exact same value here.
 */
@Serializable
data class AuthenticateAccountParams(
    val client_id: String,
    val client_secret: String,
    val redirect_uri: String,
    val code: String,
    val scope: String,
) {
    @EncodeDefault
    val grant_type = "authorization_code"
}