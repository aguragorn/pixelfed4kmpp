package me.aguragorn.pixelfed4kmpp.lib.reports.api

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/api/v1/reports")
class ReportsV1