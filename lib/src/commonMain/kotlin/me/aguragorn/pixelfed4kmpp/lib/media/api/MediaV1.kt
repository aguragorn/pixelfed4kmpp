package me.aguragorn.pixelfed4kmpp.lib.media.api

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/api/v1/media")
class MediaV1