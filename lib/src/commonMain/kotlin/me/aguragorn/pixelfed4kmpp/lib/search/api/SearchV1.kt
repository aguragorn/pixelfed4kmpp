package me.aguragorn.pixelfed4kmpp.lib.search.api

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/api/v2/search")
internal class SearchV1