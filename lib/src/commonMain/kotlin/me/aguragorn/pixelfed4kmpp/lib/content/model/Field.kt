package me.aguragorn.pixelfed4kmpp.lib.content.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class Field(
    val name: String? = null,
    val value: String? = null,
    val verified_at: Instant? = null,
)