package me.aguragorn.pixelfed4kmpp.lib.content.model

import kotlinx.serialization.Serializable

@Serializable
data class Source(
    val note: String? = null,
    val fields: List<Field>? = null,
    val privacy: Visibility? = null,
    val sensitive: Boolean? = null,
    val language: String? = null,
    val follow_requests_count: Int? = null,
)