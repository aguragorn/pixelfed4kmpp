package me.aguragorn.pixelfed4kmpp.lib.content.model

enum class Visibility {
    public, unlisted, private, direct
}