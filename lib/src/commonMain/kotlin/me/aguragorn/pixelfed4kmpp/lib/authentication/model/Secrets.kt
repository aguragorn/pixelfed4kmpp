package me.aguragorn.pixelfed4kmpp.lib.authentication.model

data class Secrets(
    val clientId: String,
    val clientSecret: String,
    val lastToken: Token,
)