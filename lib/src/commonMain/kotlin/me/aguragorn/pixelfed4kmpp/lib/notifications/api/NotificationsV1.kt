package me.aguragorn.pixelfed4kmpp.lib.notifications.api

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/api/v1/notifications")
internal class NotificationsV1