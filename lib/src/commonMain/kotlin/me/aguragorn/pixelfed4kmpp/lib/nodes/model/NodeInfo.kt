package me.aguragorn.pixelfed4kmpp.lib.nodes.model

import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.utils.isValidUrl

@Serializable
data class NodeInfo(
    val version: String? = null,
    val software: Software? = null,
    val protocols: List<String>? = null,
    val openRegistrations: Boolean? = null,
    val metadata: PixelfedMetadata? = null,
) {
    /**
     * Check if this NodeInfo has the fields we need or if we also need to look into the
     * /api/v1/instance endpoint
     * This only checks for values that might be in the /api/v1/instance endpoint.
     */
    fun hasInstanceEndpointInfo(): Boolean {
        return metadata?.config?.site?.url.isValidUrl()
                && !metadata?.config?.site?.name.isNullOrBlank()
                && metadata?.config?.uploader?.max_caption_length?.toIntOrNull() != null
    }


    @Serializable
    data class Software(
        val name: String? = null,
        val version: String? = null,
    )

    @Serializable
    data class PixelfedMetadata(
        val nodeName: String? = null,
        val software: Software? = null,
        val config: PixelfedConfig
    ) {
        @Serializable
        data class Software(
            val homepage: String? = null,
            val repo: String? = null,
        )
    }

    @Serializable
    data class PixelfedConfig(
        val open_registration: Boolean? = null,
        val uploader: Uploader? = null,
        val activitypub: ActivityPub? = null,
        val features: Features? = null,
        val site: Site? = null,
    ) {
        @Serializable
        data class Uploader(
            val max_photo_size: String? = null,
            val max_caption_length: String? = null,
            val album_limit: String? = null,
            val image_quality: String? = null,
            val optimize_image: Boolean? = null,
            val optimize_video: Boolean? = null,
            val media_types: String? = null,
            val enforce_account_limit: Boolean? = null,
        )

        @Serializable
        data class ActivityPub(
            val enabled: Boolean? = null,
            val remote_follow: Boolean? = null,
        )

        @Serializable
        data class Features(
            val mobile_apis: Boolean? = null,
            val circles: Boolean? = null,
            val stories: Boolean? = null,
            val video: Boolean? = null,
        )

        @Serializable
        data class Site(
            val name: String? = null,
            val domain: String? = null,
            val url: String? = null,
            val description: String? = null,
        )
    }
}