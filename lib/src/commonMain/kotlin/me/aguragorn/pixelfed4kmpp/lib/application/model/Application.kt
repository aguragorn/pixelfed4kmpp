package me.aguragorn.pixelfed4kmpp.lib.application.model

import kotlinx.serialization.Serializable

@Serializable
data class Application(
    //Required attributes
    val name: String,
    //Optional attributes
    val website: String? = null,
    val vapid_key: String? = null,
    //Client Attributes
    val client_id: String? = null,
    val client_secret: String? = null
)

