package me.aguragorn.pixelfed4kmpp.lib.content.model

import kotlinx.serialization.Serializable

@Serializable
data class Tag(
    //Base attributes
    val name: String,
    val url: String,
    //Optional attributes
    val history: List<History>? = emptyList()
)