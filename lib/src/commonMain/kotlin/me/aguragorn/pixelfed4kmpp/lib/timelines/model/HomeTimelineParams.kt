package me.aguragorn.pixelfed4kmpp.lib.timelines.model

import kotlinx.serialization.Serializable

/**
 * @param local If true, return only local statuses. Defaults to false.
 * @param only_media If true, return only statuses with media attachments. Defaults to false.
 * @param max_id Return results older than this ID.
 * @param since_id Return results newer than this ID.
 * @param min_id Return results immediately newer than this ID.
 * @param limit Maximum number of results to return. Defaults to 20.
 */
@Serializable
data class HomeTimelineParams(
    val local: Boolean = false,
    val max_id: String? = null,
    val since_id: String? = null,
    val min_id: String? = null,
    val limit: Int = 20,
)