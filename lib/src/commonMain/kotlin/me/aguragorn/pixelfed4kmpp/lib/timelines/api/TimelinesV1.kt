package me.aguragorn.pixelfed4kmpp.lib.timelines.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.content.model.Status
import me.aguragorn.pixelfed4kmpp.lib.timelines.model.HomeTimelineParams

@Serializable
@Resource("/api/v1/timelines")
internal class TimelinesV1 {

    @Serializable
    @Resource("public")
    class Public(val parent: TimelinesV1 = TimelinesV1())

    @Serializable
    @Resource("home")
    class Home(val parent: TimelinesV1 = TimelinesV1())

    @Serializable
    @Resource("tag/{hashtag}")
    class Tag(val parent: TimelinesV1 = TimelinesV1(), val hashtag: String)
}

/**
 * View statuses from followed users.
 */
suspend fun PixelfedSecureClient.getHomeTimeline(
    params: HomeTimelineParams
): List<Status> {
    val response = client.get(TimelinesV1.Home()) {
        setBody(params)
    }
    return response.body()
}