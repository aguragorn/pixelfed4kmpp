package me.aguragorn.pixelfed4kmpp.lib

import io.ktor.client.*

interface PixelfedClient {
    val client: HttpClient
}