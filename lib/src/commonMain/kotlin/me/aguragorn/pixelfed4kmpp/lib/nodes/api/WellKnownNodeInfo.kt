package me.aguragorn.pixelfed4kmpp.lib.nodes.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.nodes.model.NodeInfoJRD

@Serializable
@Resource("/.well-known/nodeinfo")
class WellKnownNodeInfo

suspend fun PixelfedPublicClient.getWellKnownNodeInfo(): NodeInfoJRD {
    return client.get(resource = WellKnownNodeInfo()).body()
}