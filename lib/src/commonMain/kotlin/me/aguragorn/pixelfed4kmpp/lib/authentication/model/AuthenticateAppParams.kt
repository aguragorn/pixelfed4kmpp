package me.aguragorn.pixelfed4kmpp.lib.authentication.model

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.utils.outOfBandUri

/**
 * Parameters for request to authenticate application.
 *
 * @param client_id generated when application was registered.
 * @param client_secret generated when application was registered.
 */
@Suppress("PropertyName", "unused")
@Serializable
data class AuthenticateAppParams(
    val client_id: String,
    val client_secret: String,
) {
    @EncodeDefault
    val redirect_uri = outOfBandUri

    @EncodeDefault
    val grant_type = "client_credentials"
}