package me.aguragorn.pixelfed4kmpp.lib.application.api

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.resources.*
import kotlinx.serialization.Serializable
import me.aguragorn.pixelfed4kmpp.lib.PixelfedClient
import me.aguragorn.pixelfed4kmpp.lib.application.model.Instance

@Serializable
@Resource("/api/v1/instance")
internal class InstanceV1

suspend fun PixelfedClient.getInstance(): Instance {
    return client.get(resource = InstanceV1()).body()
}