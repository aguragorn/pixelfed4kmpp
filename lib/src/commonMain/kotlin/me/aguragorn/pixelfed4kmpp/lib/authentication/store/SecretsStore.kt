package me.aguragorn.pixelfed4kmpp.lib.authentication.store

import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Secrets

interface SecretsStore {
    suspend fun save(secrets: Secrets)
    suspend fun get(): Secrets
}