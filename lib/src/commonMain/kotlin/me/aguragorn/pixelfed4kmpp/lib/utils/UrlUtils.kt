package me.aguragorn.pixelfed4kmpp.lib.utils

import io.ktor.http.*

internal const val outOfBandUri = "urn:ietf:wg:oauth:2.0:oob"

/**
 * Check if domain is valid or not
 */
internal fun String?.isValidUrl(): Boolean {
    if (this == null) return false

    return try {
        Url(this)
        true
    } catch (e: URLParserException) {
        false
    }
}

/**
 * Returns the same [String] if it is a valid URL
 * else it is appended as relative path to the [baseUrl].
 */
internal fun String.resolveOn(
    baseUrl: String
): String = takeIf { it.isValidUrl() } ?: "$baseUrl${asRelativePath()}"

/**
 * Returns the same [String] but with a path separator `"/"` at the start.
 */
internal fun String.asRelativePath() = takeIf { it.startsWith("/") } ?: "/$this"