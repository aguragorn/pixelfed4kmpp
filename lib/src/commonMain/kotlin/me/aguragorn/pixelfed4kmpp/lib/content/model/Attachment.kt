package me.aguragorn.pixelfed4kmpp.lib.content.model

import kotlinx.serialization.Serializable

@Serializable
data class Attachment(
    //Required attributes
    val id: String?,
    val type: AttachmentType? = AttachmentType.image,
    val url: String?,
    val preview_url: String? = "",
    //Optional attributes
    val remote_url: String? = null,
    val text_url: String? = null,

    val meta: Meta?,

    val description: String? = null,
    val blurhash: String? = null
) {

    enum class AttachmentType {
        unknown, image, gifv, video, audio
    }

    @Serializable
    data class Meta(
        val focus: Focus?,
        val original: Image?
    ) {

        @Serializable
        data class Focus(
            val x: Double?,
            val y: Double?
        )

        @Serializable
        data class Image(
            val width: Int?,
            val height: Int?,
            val size: String?,
            val aspect: Double?
        )
    }
}