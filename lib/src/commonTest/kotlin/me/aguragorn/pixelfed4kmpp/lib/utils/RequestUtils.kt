package me.aguragorn.pixelfed4kmpp.lib.utils

import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*

val HttpRequestData.formDataContent: Parameters? get() = (body as? FormDataContent)?.formData