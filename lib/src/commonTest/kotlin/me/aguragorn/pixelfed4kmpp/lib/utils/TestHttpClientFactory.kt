package me.aguragorn.pixelfed4kmpp.lib.utils

import io.ktor.client.*
import io.ktor.client.engine.*


class TestHttpClientFactory(
    private val engine: HttpClientEngine
) : HttpClientFactory {
    override fun newInstance(): HttpClient = HttpClient(engine) {
        defaults()
    }
}